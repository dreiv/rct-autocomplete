import React from 'react'

import suggestions from 'data/suggestions'

import { Autocomplete } from 'components/auto-complete'

import './App.scss'

const App = () => (
	<>
		<h1>Autocomplete demo</h1>
		<h2>Start typing and experience the autocomplete wizardry!</h2>

		<Autocomplete 
			suggestions={suggestions}
			placeholder='Find pokemon...'
		/>
	</>
)

export default App
