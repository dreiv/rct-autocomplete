import React, { Component } from 'react'

import { SuggestionsList } from './suggestions-list'

import './Autocomplete.scss'

const SUGGESTIONS_COUNT = 4

class Autocomplete extends Component {
	static defaultProps = {
		suggestions: []
	}

	constructor(props) {
		super(props)

		this.state = {
			showSuggestions: false,
			activeSuggestionIdx: 0,
			filteredSuggestions: [],
			userInput: '',
			optimizedUserInput: ''
		}

		this.handleOnChange = this.handleOnChange.bind(this)
		this.handleOnKeyDown = this.handleOnKeyDown.bind(this)
		this.handleOnSelect = this.handleOnSelect.bind(this)
		this.handleOnBlur = this.handleOnBlur.bind(this)
	}

	handleOnChange = evt => {
		const { suggestions } = this.props
		const userInput = evt.currentTarget.value

		const optimizedUserInput = userInput.trim().toLowerCase()
		const filteredSuggestions = []

		if (optimizedUserInput.length) {
			suggestions.some(
				suggestion =>
					suggestion.startsWith(optimizedUserInput) &&
					filteredSuggestions.push(suggestion) &&
					filteredSuggestions.length >= SUGGESTIONS_COUNT
			)
		}

		this.setState({
			userInput,
			optimizedUserInput,
			showSuggestions: filteredSuggestions.length > 0,
			filteredSuggestions,
			activeSuggestionIdx: 0
		})
	}

	handleOnKeyDown = (evt) => {
		const { showSuggestions, filteredSuggestions } = this.state
		if (!showSuggestions) {
			return
		}

		switch (evt.key) {
			case "Enter":
				this.setState(prevState => ({
					activeSuggestionIdx: 0,
					showSuggestions: false,
					userInput: filteredSuggestions[prevState.activeSuggestionIdx]
				}))
				break

			case "ArrowUp":
				evt.preventDefault()
				this.setState(prevState => ({
					activeSuggestionIdx:
						(prevState.activeSuggestionIdx - 1 + filteredSuggestions.length) %
						filteredSuggestions.length
				}))
				break

			case "ArrowDown":
				this.setState(prevState => ({
					activeSuggestionIdx:
						(prevState.activeSuggestionIdx + 1) % filteredSuggestions.length
				}))
				break
			default:
				break
		}
	}

	handleOnSelect = suggestion => () => {
		this.setState({
			userInput: suggestion,
			showSuggestions: false
		})
	}

	handleOnBlur = () => {
		this.setState({ showSuggestions: false })
	}

	render() {
		const {
			handleOnChange,
			handleOnSelect,
			handleOnKeyDown,
			handleOnBlur,
			props: {
				suggestions,
				...propsRest
			},
			state: {
				userInput,
				optimizedUserInput,
				filteredSuggestions,
				activeSuggestionIdx,
				showSuggestions
			}
		} = this

		return (
			<div className="autocomplete">
				<input
					type="text"
					onChange={handleOnChange}
					value={userInput}
					onBlur={handleOnBlur}
					onKeyDown={handleOnKeyDown}
					{...propsRest}
				/>

				{showSuggestions && (
					<SuggestionsList
						optimizedUserInput={optimizedUserInput}
						handleOnSelect={handleOnSelect}
						filteredSuggestions={filteredSuggestions}
						activeSuggestionIdx={activeSuggestionIdx}
					/>
				)}
			</div>
		)
	}
}

export default Autocomplete
