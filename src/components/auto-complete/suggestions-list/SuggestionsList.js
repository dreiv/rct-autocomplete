import React from 'react'

import './SuggestionsList.scss'

const SuggestionsList = ({
	filteredSuggestions,
	activeSuggestionIdx,
	handleOnSelect,
	optimizedUserInput
}) => (
	<ul className='suggestions'>
		{filteredSuggestions.map((suggestion, idx) => {
			let className

			if (activeSuggestionIdx === idx) {
				className = "suggestion-active";
			}
			const matchRest = suggestion.slice(optimizedUserInput.length)

			return (
				<li
					key={suggestion}
					className={className}
					onMouseDown={handleOnSelect(suggestion)}
				>
					{optimizedUserInput}<b>{matchRest}</b>
				</li>
			)
		})}
	</ul>
)

export default SuggestionsList
